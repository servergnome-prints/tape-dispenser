# Tape Dispenser

Warning: I've based this design on anoter design I found either on Thingiverse, Printables or Thangs, but I can't find it anymore. If you know the original author, please let me know so I can give credit where credit is due. You can contact me using the [Issues tab in GitLab](https://gitlab.com/servergnome-prints/tape-dispenser/-/issues).

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Licensed under [Unlicense](https://gitlab.com/servergnome-prints/tape-dispenser/-/blob/main/LICENSE)

